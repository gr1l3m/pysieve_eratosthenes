from pprint import pprint

def sieve_eratosthenes(n):
    A = {i:True for i in range(2, n+1)}
    for i, is_prime in A.items():
        if is_prime:
            for j in range(i*2, n+1, i):
                A[j] = False
    return A


if __name__ == "__main__":
    primes_dict = sieve_eratosthenes(600)
    pprint(f"{primes_dict=}")
    primes_list = [i for i, is_prime in primes_dict.items() if is_prime]
    pprint(f"{primes_list=}")