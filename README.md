# pysieve_eratosthenes

Python readable implementation of the sieve of Eratosthenes to find the prime numbers in a range of numbers.

The function returns a dictionary with each integer as the key, and a boolean whether the integer is prime or not. A list comprehension is used to get a list of the prime integers.